import { Component } from '@angular/core';
import { rejects } from 'assert';
import { resolve } from 'dns';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'promise';

  id = ''
  items = {
    1: 'apple',
    2: 'banana',
    3: 'mango'
  }
  ngOnInit(){
  
      this.getid().then((data)=>{
        this.id = data
        console.log('The id you entered is ' ,data)
        this.getitems(this.id).then((data)=>{
          console.log('The fruit is ',data)
        })
      })

    
  }

  private getid(): Promise<string>{
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        resolve('3')
      }, 1000)
    })
  }

  private getitems(x): Promise<number>{
    return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        resolve(this.items[x])
      }, 1000)
    })
  }

}
